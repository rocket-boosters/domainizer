import pytest

import domainize


CASES = [
    ("l.fabricadeprovas.com.br", "fabricadeprovas.com.br"),
    ("crm.webfletch.co.uk", "webfletch.co.uk"),
    ("en.corp.datagrav.com", "datagrav.com"),
    ("https://www.crunchbase.com/organization/mattermark", "crunchbase.com"),
    ("https://www.crunchbase.com:443/organization/mattermark", "crunchbase.com"),
    ("crunchbase.com:443", "crunchbase.com"),
]


@pytest.mark.parametrize("incoming,expected", CASES)
def test_domainize(incoming: str, expected: str):
    """Should be able to run main_cli without error."""
    assert expected == domainize.get_domain(incoming)
